# Plan Climat Air Énergie Territorial

PCAET: https://fr.wikipedia.org/wiki/Plan_climat-air-%C3%A9nergie_territorial



Le but du projet est de fournir une plateforme permettant aux citoyens d'organiser des rencontres, co-rédiger, et de suivre la mise en application du PCAET.

## Objectifs fonctionnels

#### Se rencontrer, débatre :
- permettre aux citoyens d'un territoire de créer ou de rejoindre un groupe local,
- permettre aux citoyens d'animer ou d'assister à une rencontre ou un débat,
- permettre aux citoyens de consulter les comptes rendus de réunions, rencontres ou débats
- permettre aux citoyens d'identifiers les acteurs locaux s'engageant dans le PCAET

#### Co-rédiger :
- permettre aux citoyens d'intégrer un PCAET existant
- permettre aux citoyens de co-rédiger le PCAET

#### Suivre la mise en applicaion :
- mettre en forme les données provenant d'indiquateurs mesurables attestant du respect du PCAET


## Technologies, objectifs techniques

- Ruby on Rails
- Bootstrap (custom scss)
- AJAX
- Responsive design

#### rails recommandations

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
